FROM alpine:3.15.0

RUN apk add --no-cache curl=7.80.0-r0 tar=1.34-r0 && \
    rm -rf /var/cache/apk/*
